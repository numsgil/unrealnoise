#pragma once

#include "NoiseModule.h"
#include "AbsModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UAbsModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UAbsModule();
	virtual ~UAbsModule();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
