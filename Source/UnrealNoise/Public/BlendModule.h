#pragma once

#include "NoiseModule.h"
#include "BlendModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UBlendModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UBlendModule();
	virtual ~UBlendModule();

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual UNoiseModule* GetControlModule() const;

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void SetControlModule(UNoiseModule* Control);

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
