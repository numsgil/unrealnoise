#pragma once

#include "NoiseModule.h"
#include "RotatePointModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API URotatePointModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	URotatePointModule();
	virtual ~URotatePointModule();

	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float XAngle = 0.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float YAngle = 0.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float ZAngle = 0.f;

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void CalculateMatrix();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;

protected:
	UPROPERTY()
	float X1Matrix;
	UPROPERTY()
	float X2Matrix;
	UPROPERTY()
	float X3Matrix;
	UPROPERTY()
	float Y1Matrix;
	UPROPERTY()
	float Y2Matrix;
	UPROPERTY()
	float Y3Matrix;
	UPROPERTY()
	float Z1Matrix;
	UPROPERTY()
	float Z2Matrix;
	UPROPERTY()
	float Z3Matrix;
};
