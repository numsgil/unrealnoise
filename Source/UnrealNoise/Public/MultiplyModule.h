#pragma once

#include "NoiseModule.h"
#include "MultiplyModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UMultiplyModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UMultiplyModule();
	virtual ~UMultiplyModule();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
