#pragma once

#include "NoiseModule.h"
#include "InvertModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UInvertModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UInvertModule();
	virtual ~UInvertModule();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
