#pragma once

#include "NoiseModule.h"
#include "PerlinModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UPerlinModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UPerlinModule();
	virtual ~UPerlinModule();

	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float Frequency = 1.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float Lacunarity = 2.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float Persistence = 0.5f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise", meta = (ClampMin = "1", ClampMax = "30", UIMin = "1", UIMax = "30"))
	int32 OctaveCount = 6;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	int32 Seed = 0;

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
