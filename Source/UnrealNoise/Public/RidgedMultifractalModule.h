#pragma once

#include "NoiseModule.h"
#include "RidgedMultifractalModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API URidgedMultifractalModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	URidgedMultifractalModule();
	virtual ~URidgedMultifractalModule();

	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float Frequency = 1.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float Lacunarity = 2.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise", meta = (ClampMin = "1", ClampMax = "30", UIMin = "1", UIMax = "30"))
	int32 OctaveCount = 6;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	int32 Seed = 0;

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void CalculateSpectralWeights();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;

protected:
	UPROPERTY()
	float SpectralWeights[30];
};
