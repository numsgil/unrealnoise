#pragma once

#include "NoiseModule.h"
#include "ClampModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UClampModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UClampModule();
	virtual ~UClampModule();

	UPROPERTY(BlueprintReadWrite)
	float LowerBound = -1.f;
	UPROPERTY(BlueprintReadWrite)
	float UpperBound = 1.f;

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
