#pragma once

#include "NoiseModule.h"
#include "CheckerboardModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UCheckerboardModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UCheckerboardModule();
	virtual ~UCheckerboardModule();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
