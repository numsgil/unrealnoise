#pragma once

#include "NoiseModule.h"
#include "ConstantModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UConstantModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UConstantModule();
	virtual ~UConstantModule();

	UPROPERTY(BlueprintReadWrite)
	float Value = 0.f;

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
