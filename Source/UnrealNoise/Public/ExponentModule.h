#pragma once

#include "NoiseModule.h"
#include "ExponentModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UExponentModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UExponentModule();
	virtual ~UExponentModule();

	UPROPERTY(BlueprintReadWrite)
	float Exponent = 1.f;

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
