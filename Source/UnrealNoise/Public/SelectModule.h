#pragma once

#include "NoiseModule.h"
#include "SelectModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API USelectModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	USelectModule();
	virtual ~USelectModule();

	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float LowerBound = -1.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float UpperBound = 1.f;

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void SetControlModule(UNoiseModule* Control);

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual UNoiseModule* GetControlModule() const;

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void SetEdgeFalloff(float Falloff);

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;

protected:
	UPROPERTY()
	float EdgeFalloff;
};
