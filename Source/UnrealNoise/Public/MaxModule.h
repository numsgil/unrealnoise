#pragma once

#include "NoiseModule.h"
#include "MaxModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UMaxModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UMaxModule();
	virtual ~UMaxModule();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
