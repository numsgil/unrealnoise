#pragma once

#include "NoiseModule.h"
#include "MinModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UMinModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UMinModule();
	virtual ~UMinModule();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
