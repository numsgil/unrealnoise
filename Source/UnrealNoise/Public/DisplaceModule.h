#pragma once

#include "NoiseModule.h"
#include "DisplaceModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UDisplaceModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UDisplaceModule();
	virtual ~UDisplaceModule();

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual UNoiseModule* GetXDisplaceModule() const;

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void SetXDisplaceModule(UNoiseModule* DisplaceModule);

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual UNoiseModule* GetYDisplaceModule() const;

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void SetYDisplaceModule(UNoiseModule* DisplaceModule);

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual UNoiseModule* GetZDisplaceModule() const;

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void SetZDisplaceModule(UNoiseModule* DisplaceModule);

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void SetDisplaceModules(UNoiseModule* XDisplace, UNoiseModule* YDisplace, UNoiseModule* ZDisplace)
	{
		SetXDisplaceModule(XDisplace);
		SetYDisplaceModule(YDisplace);
		SetZDisplaceModule(ZDisplace);
	}

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
