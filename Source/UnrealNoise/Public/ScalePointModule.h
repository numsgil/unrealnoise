#pragma once

#include "NoiseModule.h"
#include "ScalePointModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UScalePointModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UScalePointModule();
	virtual ~UScalePointModule();

	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float XScale = 1.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float YScale = 1.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float ZScale = 1.f;

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
