#pragma once

#include "Object.h"
#include "Array.h"
#include "NoiseModule.generated.h"

UCLASS(BlueprintType, Abstract)
class UNREALNOISE_API UNoiseModule : public UObject
{
	GENERATED_BODY()

public:
	UNoiseModule();
	UNoiseModule(int32 SourceModuleCount);
	virtual ~UNoiseModule();

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual UNoiseModule* GetSourceModule(int32 Index) const;

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual int32 GetSourceModuleCount() const { unimplemented(); return 0; }

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual float GetValue(float X, float Y, float Z) const { unimplemented(); return 0.f; }

	UFUNCTION(BlueprintCallable, Category = "Unreal Noise")
	virtual void SetSourceModule(int32 Index, UNoiseModule* Source);

protected:
	UPROPERTY()
	TArray<UNoiseModule*> Modules;
};
