#pragma once

#include "NoiseModule.h"
#include "PowerModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UPowerModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UPowerModule();
	virtual ~UPowerModule();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
