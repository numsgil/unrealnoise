#pragma once

#include "NoiseModule.h"
#include "ScaleBiasModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UScaleBiasModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UScaleBiasModule();
	virtual ~UScaleBiasModule();

	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float Scale = 1.f;
	UPROPERTY(BlueprintReadWrite, Category = "Unreal Noise")
	float Bias = 0.f;

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
