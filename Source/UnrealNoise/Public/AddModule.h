#pragma once

#include "NoiseModule.h"
#include "AddModule.generated.h"

UCLASS(BlueprintType)
class UNREALNOISE_API UAddModule : public UNoiseModule
{
	GENERATED_BODY()

public:
	UAddModule();
	virtual ~UAddModule();

	virtual int32 GetSourceModuleCount() const override;

	virtual float GetValue(float X, float Y, float Z) const override;
};
