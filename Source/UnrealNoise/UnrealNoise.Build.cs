// Some copyright should be here...

using UnrealBuildTool;

public class UnrealNoise : ModuleRules
{
	public UnrealNoise(TargetInfo Target)
	{
		PublicIncludePaths.AddRange(new string[] 
		{
			"UnrealNoise/Public"
		});
				
		
		PrivateIncludePaths.AddRange(new string[] 
		{
			"UnrealNoise/Private"
		});
			
		
		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Engine",
			"Core",
			"CoreUObject"
		});
			
		
		PrivateDependencyModuleNames.AddRange(new string[]
		{
			"Slate", "SlateCore"
		});
	}
}
