#pragma once

#include "UnrealMath.h"

const int32 X_NOISE_GEN = 1619;
const int32 Y_NOISE_GEN = 31337;
const int32 Z_NOISE_GEN = 6971;
const int32 SEED_NOISE_GEN = 1013;
const int32 SHIFT_NOISE_GEN = 8;

extern float g_randomVectors[256 * 4];

/// Modifies a floating-point value so that it can be stored in a
/// noise::int32 variable.
///
/// @param n A floating-point number.
///
/// @returns The modified floating-point number.
///
/// This function does not modify @a n.
///
/// In libnoise, the noise-generating algorithms are all integer-based;
/// they use variables of type noise::int32.  Before calling a noise
/// function, pass the @a x, @a y, and @a z coordinates to this function to
/// ensure that these coordinates can be cast to a noise::int32 value.
///
/// Although you could do a straight cast from double to noise::int32, the
/// resulting value may differ between platforms.  By using this function,
/// you ensure that the resulting value is identical between platforms.
FORCEINLINE float MakeInt32Range(float n)
{
	if (n >= 1073741824.f)
		return (2.f * FMath::Fmod(n, 1073741824.f)) - 1073741824.f;
	else if (n <= -1073741824.f)
		return (2.f * FMath::Fmod(n, 1073741824.f)) + 1073741824.f;
	return n;
}

/// Maps a value onto a cubic S-curve.
///
/// @param a The value to map onto a cubic S-curve.
///
/// @returns The mapped value.
///
/// @a a should range from 0.0 to 1.0.
///
/// The derivitive of a cubic S-curve is zero at @a a = 0.0 and @a a =
/// 1.0
FORCEINLINE float SCurve3(float a)
{
	return a * a * (3.f - 2.f * a);
}

/// Performs linear interpolation between two values.
///
/// @param n0 The first value.
/// @param n1 The second value.
/// @param a The alpha value.
///
/// @returns The interpolated value.
///
/// The alpha value should range from 0.0 to 1.0.  If the alpha value is
/// 0.0, this function returns @a n0.  If the alpha value is 1.0, this
/// function returns @a n1.
FORCEINLINE float LinearInterp(float n0, float n1, float a)
{
	return ((1.0 - a) * n0) + (a * n1);
}

FORCEINLINE float GradientNoise3D(float fx, float fy, float fz, int32 ix, int32 iy, int32 iz, int32 seed)
{
	// Randomly generate a gradient vector given the integer coordinates of the
	// input value.  This implementation generates a random number and uses it
	// as an index into a normalized-vector lookup table.
	int32 vectorIndex = (
		X_NOISE_GEN    * ix
		+ Y_NOISE_GEN    * iy
		+ Z_NOISE_GEN    * iz
		+ SEED_NOISE_GEN * seed)
		& 0xffffffff;
	vectorIndex ^= (vectorIndex >> SHIFT_NOISE_GEN);
	vectorIndex &= 0xff;

	float xvGradient = g_randomVectors[(vectorIndex << 2)];
	float yvGradient = g_randomVectors[(vectorIndex << 2) + 1];
	float zvGradient = g_randomVectors[(vectorIndex << 2) + 2];

	// Set up us another vector equal to the distance between the two vectors
	// passed to this function.
	float xvPoint = (fx - (float)ix);
	float yvPoint = (fy - (float)iy);
	float zvPoint = (fz - (float)iz);

	// Now compute the dot product of the gradient vector with the distance
	// vector.  The resulting value is gradient noise.  Apply a scaling value
	// so that this noise value ranges from -1.0 to 1.0.
	return ((xvGradient * xvPoint)
		+ (yvGradient * yvPoint)
		+ (zvGradient * zvPoint)) * 2.12f;
}

FORCEINLINE float GradientCoherentNoise3D(float x, float y, float z, int32 seed)
{
	// Create a unit-length cube aligned along an integer boundary.  This cube
	// surrounds the input point.
	int32 x0 = (x > 0.0 ? (int32)x : (int32)x - 1);
	int32 x1 = x0 + 1;
	int32 y0 = (y > 0.0 ? (int32)y : (int32)y - 1);
	int32 y1 = y0 + 1;
	int32 z0 = (z > 0.0 ? (int32)z : (int32)z - 1);
	int32 z1 = z0 + 1;

	// Map the difference between the coordinates of the input value and the
	// coordinates of the cube's outer-lower-left vertex onto an S-curve.
	float xs = SCurve3(x - (float)x0);
	float ys = SCurve3(y - (float)y0);
	float zs = SCurve3(z - (float)z0);
	
	// Now calculate the noise values at each vertex of the cube.  To generate
	// the coherent-noise value at the input point, interpolate these eight
	// noise values using the S-curve value as the interpolant (trilinear
	// interpolation.)
	float n0, n1, ix0, ix1, iy0, iy1;
	n0 = GradientNoise3D(x, y, z, x0, y0, z0, seed);
	n1 = GradientNoise3D(x, y, z, x1, y0, z0, seed);
	ix0 = LinearInterp(n0, n1, xs);
	n0 = GradientNoise3D(x, y, z, x0, y1, z0, seed);
	n1 = GradientNoise3D(x, y, z, x1, y1, z0, seed);
	ix1 = LinearInterp(n0, n1, xs);
	iy0 = LinearInterp(ix0, ix1, ys);
	n0 = GradientNoise3D(x, y, z, x0, y0, z1, seed);
	n1 = GradientNoise3D(x, y, z, x1, y0, z1, seed);
	ix0 = LinearInterp(n0, n1, xs);
	n0 = GradientNoise3D(x, y, z, x0, y1, z1, seed);
	n1 = GradientNoise3D(x, y, z, x1, y1, z1, seed);
	ix1 = LinearInterp(n0, n1, xs);
	iy1 = LinearInterp(ix0, ix1, ys);

	return LinearInterp(iy0, iy1, zs);
}
