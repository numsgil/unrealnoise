#include "UnrealNoisePrivatePCH.h"
#include "RotatePointModule.h"

URotatePointModule::URotatePointModule() : UNoiseModule(GetSourceModuleCount())
{
}

URotatePointModule::~URotatePointModule()
{
}

void URotatePointModule::CalculateMatrix()
{
	float xCos, yCos, zCos, xSin, ySin, zSin;
	xCos = FMath::Cos(FMath::DegreesToRadians(XAngle));
	yCos = FMath::Cos(FMath::DegreesToRadians(YAngle));
	zCos = FMath::Cos(FMath::DegreesToRadians(ZAngle));
	xSin = FMath::Sin(FMath::DegreesToRadians(XAngle));
	ySin = FMath::Sin(FMath::DegreesToRadians(YAngle));
	zSin = FMath::Sin(FMath::DegreesToRadians(ZAngle));

	X1Matrix = ySin * xSin * zSin + yCos * zCos;
	Y1Matrix = xCos * zSin;
	Z1Matrix = ySin * zCos - yCos * xSin * zSin;
	X2Matrix = ySin * xSin * zCos - yCos * zSin;
	Y2Matrix = xCos * zCos;
	Z2Matrix = -yCos * xSin * zCos - ySin * zSin;
	X3Matrix = -ySin * xCos;
	Y3Matrix = xSin;
	Z3Matrix = yCos * xCos;
}

int32 URotatePointModule::GetSourceModuleCount() const
{
	return 1;
}

float URotatePointModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);

	float nx = (X1Matrix * X) + (Y1Matrix * Y) + (Z1Matrix * Z);
	float ny = (X2Matrix * X) + (Y2Matrix * Y) + (Z2Matrix * Z);
	float nz = (X3Matrix * X) + (Y3Matrix * Y) + (Z3Matrix * Z);
	return Modules[0]->GetValue(nx, ny, nz);
}
