#include "UnrealNoisePrivatePCH.h"
#include "NoiseModule.h"

UNoiseModule::UNoiseModule()
{
}

UNoiseModule::UNoiseModule(int32 SourceModuleCount)
{
	for (int32 i = 0; i < SourceModuleCount; i++)
		Modules.AddZeroed();
}

UNoiseModule::~UNoiseModule()
{
}

UNoiseModule* UNoiseModule::GetSourceModule(int32 Index) const
{
	check(Modules.Num() > 0 && Index >= 0);
	check(Index < Modules.Num() && Modules[Index] != nullptr);

	return Modules[Index];
}

void UNoiseModule::SetSourceModule(int32 Index, UNoiseModule* Source)
{
	check(Modules.Num() > 0 && Index >= 0);
	check(Index < Modules.Num() && Source != nullptr);

	Modules[Index] = Source;
}
