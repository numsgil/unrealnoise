#include "UnrealNoisePrivatePCH.h"
#include "ScaleBiasModule.h"

UScaleBiasModule::UScaleBiasModule() : UNoiseModule(GetSourceModuleCount())
{
}

UScaleBiasModule::~UScaleBiasModule()
{
}

int32 UScaleBiasModule::GetSourceModuleCount() const
{
	return 1;
}

float UScaleBiasModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	return Modules[0]->GetValue(X, Y, Z) * Scale + Bias;
}
