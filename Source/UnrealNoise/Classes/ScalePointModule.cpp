#include "UnrealNoisePrivatePCH.h"
#include "ScalePointModule.h"

UScalePointModule::UScalePointModule() : UNoiseModule(GetSourceModuleCount())
{
}

UScalePointModule::~UScalePointModule()
{
}

int32 UScalePointModule::GetSourceModuleCount() const
{
	return 1;
}

float UScalePointModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	
	return Modules[0]->GetValue(X * XScale, Y * YScale, Z * ZScale);
}
