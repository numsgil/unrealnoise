#include "UnrealNoisePrivatePCH.h"
#include "PerlinModule.h"

UPerlinModule::UPerlinModule()
{
}

UPerlinModule::~UPerlinModule()
{
}

int32 UPerlinModule::GetSourceModuleCount() const
{
	return 0;
}

float UPerlinModule::GetValue(float X, float Y, float Z) const
{
	float value = 0.f, signal = 0.f, curPersistence = 1.f, nx, ny, nz;
	int32 seed;

	X *= Frequency;
	Y *= Frequency;
	Z *= Frequency;

	for (int32 curOctave = 0; curOctave < OctaveCount; curOctave++)
	{
		// Make sure that these floating-point values have the same range as a 32-
		// bit integer so that we can pass them to the coherent-noise functions.
		nx = MakeInt32Range(X);
		ny = MakeInt32Range(Y);
		nz = MakeInt32Range(Z);

		// Get the coherent-noise value from the input value and add it to the
		// final result.
		seed = (Seed + curOctave) & 0xffffffff;
		signal = GradientCoherentNoise3D(nx, ny, nz, seed);
		value += signal * curPersistence;

		// Prepare the next octave.
		X *= Lacunarity;
		Y *= Lacunarity;
		Z *= Lacunarity;
		curPersistence *= Persistence;
	}

	return value;
}
