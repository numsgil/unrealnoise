#include "UnrealNoisePrivatePCH.h"
#include "SelectModule.h"

USelectModule::USelectModule() : UNoiseModule(GetSourceModuleCount())
{
	SetEdgeFalloff(0.f);
}

USelectModule::~USelectModule()
{
}

void USelectModule::SetControlModule(UNoiseModule* Control)
{
	Modules[2] = Control;
}

UNoiseModule* USelectModule::GetControlModule() const
{
	return Modules[2];
}

void USelectModule::SetEdgeFalloff(float Falloff)
{
	// Make sure that the edge falloff curves do not overlap.
	float boundSize = UpperBound - LowerBound;
	EdgeFalloff = (Falloff > boundSize / 2) ? boundSize / 2 : Falloff;
}

int32 USelectModule::GetSourceModuleCount() const
{
	return 3;
}

float USelectModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	check(Modules[1] != nullptr);
	check(Modules[2] != nullptr);
	
	float controlValue = Modules[2]->GetValue(X, Y, Z), alpha;
	if (EdgeFalloff > 0.f)
	{
		if (controlValue < LowerBound - EdgeFalloff)
		{
			// The output value from the control module is below the selector
			// threshold; return the output value from the first source module.
			return Modules[0]->GetValue(X, Y, Z);
		}
		else if (controlValue < LowerBound + EdgeFalloff)
		{
			// The output value from the control module is near the lower end of the
			// selector threshold and within the smooth curve. Interpolate between
			// the output values from the first and second source modules.
			float lowerCurve = (LowerBound - EdgeFalloff);
			float upperCurve = (LowerBound + EdgeFalloff);
			alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));
			return LinearInterp(Modules[0]->GetValue(X, Y, Z), Modules[1]->GetValue(X, Y, Z), alpha);
		}
		else if (controlValue < UpperBound - EdgeFalloff)
		{
			// The output value from the control module is within the selector
			// threshold; return the output value from the second source module.
			return Modules[1]->GetValue(X, Y, Z);
		}
		else if (controlValue < UpperBound + EdgeFalloff)
		{
			// The output value from the control module is near the upper end of the
			// selector threshold and within the smooth curve. Interpolate between
			// the output values from the first and second source modules.
			float lowerCurve = (UpperBound - EdgeFalloff);
			float upperCurve = (UpperBound + EdgeFalloff);
			alpha = SCurve3((controlValue - lowerCurve) / (upperCurve - lowerCurve));
			return LinearInterp(Modules[1]->GetValue(X, Y, Z), Modules[0]->GetValue(X, Y, Z), alpha);
		}
		else
		{
			// Output value from the control module is above the selector threshold;
			// return the output value from the first source module.
			return Modules[0]->GetValue(X, Y, Z);
		}
	}
	else
	{
		return (controlValue < LowerBound || controlValue > UpperBound) ? Modules[0]->GetValue(X, Y, Z) : Modules[1]->GetValue(X, Y, Z);
	}
}
