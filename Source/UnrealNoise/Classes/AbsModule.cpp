#include "UnrealNoisePrivatePCH.h"
#include "AbsModule.h"

UAbsModule::UAbsModule() : UNoiseModule(GetSourceModuleCount())
{
}

UAbsModule::~UAbsModule()
{
}

int32 UAbsModule::GetSourceModuleCount() const
{
	return 1;
}

float UAbsModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);

	return FMath::Abs(Modules[0]->GetValue(X, Y, Z));
}
