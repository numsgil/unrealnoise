#include "UnrealNoisePrivatePCH.h"
#include "BillowModule.h"

UBillowModule::UBillowModule()
{
}

UBillowModule::~UBillowModule()
{
}

int32 UBillowModule::GetSourceModuleCount() const
{
	return 0;
}

float UBillowModule::GetValue(float X, float Y, float Z) const
{
	float value = 0.0;
	float signal = 0.0;
	float curPersistence = 1.0;
	float nx, ny, nz;
	int32 seed;

	X *= Frequency;
	Y *= Frequency;
	Z *= Frequency;

	for (int curOctave = 0; curOctave < OctaveCount; curOctave++)
	{
		// Make sure that these floating-point values have the same range as a 32-
		// bit integer so that we can pass them to the coherent-noise functions.
		nx = MakeInt32Range(X);
		ny = MakeInt32Range(Y);
		nz = MakeInt32Range(Z);

		// Get the coherent-noise value from the input value and add it to the
		// final result.
		seed = (Seed + curOctave) & 0xffffffff;
		signal = GradientCoherentNoise3D(nx, ny, nz, seed);
		signal = 2.0 * FMath::Abs(signal) - 1.0;
		value += signal * curPersistence;
	}

	return value + 0.5f;
}
