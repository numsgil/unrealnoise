#include "UnrealNoisePrivatePCH.h"
#include "BlendModule.h"

UBlendModule::UBlendModule() : UNoiseModule(GetSourceModuleCount())
{
}

UBlendModule::~UBlendModule()
{
}

UNoiseModule* UBlendModule::GetControlModule() const
{
	return Modules[2];
}

void UBlendModule::SetControlModule(UNoiseModule* Control)
{
	Modules[2] = Control;
}

int32 UBlendModule::GetSourceModuleCount() const
{
	return 3;
}

float UBlendModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	check(Modules[1] != nullptr);
	check(Modules[2] != nullptr);

	float v0 = Modules[0]->GetValue(X, Y, Z);
	float v1 = Modules[1]->GetValue(X, Y, Z);
	float alpha = (Modules[2]->GetValue(X, Y, Z) + 1.f) / 2.f;
	return LinearInterp(v0, v1, alpha);
}
