#include "UnrealNoisePrivatePCH.h"
#include "PowerModule.h"

UPowerModule::UPowerModule() : UNoiseModule(GetSourceModuleCount())
{
}

UPowerModule::~UPowerModule()
{
}

int32 UPowerModule::GetSourceModuleCount() const
{
	return 2;
}

float UPowerModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	check(Modules[1] != nullptr);

	return FMath::Pow(Modules[0]->GetValue(X, Y, Z), Modules[1]->GetValue(X, Y, Z));
}
