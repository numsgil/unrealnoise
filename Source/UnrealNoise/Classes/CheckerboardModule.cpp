#include "UnrealNoisePrivatePCH.h"
#include "CheckerboardModule.h"

UCheckerboardModule::UCheckerboardModule()
{
}

UCheckerboardModule::~UCheckerboardModule()
{
}

int32 UCheckerboardModule::GetSourceModuleCount() const
{
	return 0;
}

float UCheckerboardModule::GetValue(float X, float Y, float Z) const
{
	int32 ix = FMath::FloorToInt(MakeInt32Range(X));
	int32 iy = FMath::FloorToInt(MakeInt32Range(Y));
	int32 iz = FMath::FloorToInt(MakeInt32Range(Z));
	return (ix & 1 ^ iy & 1 ^ iz & 1) ? -1.f : 1.f;
}
