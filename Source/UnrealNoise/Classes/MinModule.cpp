#include "UnrealNoisePrivatePCH.h"
#include "MinModule.h"

UMinModule::UMinModule() : UNoiseModule(GetSourceModuleCount())
{
}

UMinModule::~UMinModule()
{
}

int32 UMinModule::GetSourceModuleCount() const
{
	return 2;
}

float UMinModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	check(Modules[1] != nullptr);

	return FMath::Min(Modules[0]->GetValue(X, Y, Z), Modules[1]->GetValue(X, Y, Z));
}
