#include "UnrealNoisePrivatePCH.h"
#include "DisplaceModule.h"

UDisplaceModule::UDisplaceModule() : UNoiseModule(GetSourceModuleCount())
{
}

UDisplaceModule::~UDisplaceModule()
{
}

UNoiseModule* UDisplaceModule::GetXDisplaceModule() const
{
	return Modules[1];
}

void UDisplaceModule::SetXDisplaceModule(UNoiseModule* DisplaceModule)
{
	Modules[1] = DisplaceModule;
}

UNoiseModule* UDisplaceModule::GetYDisplaceModule() const
{
	return Modules[2];
}

void UDisplaceModule::SetYDisplaceModule(UNoiseModule* DisplaceModule)
{
	Modules[2] = DisplaceModule;
}

UNoiseModule* UDisplaceModule::GetZDisplaceModule() const
{
	return Modules[3];
}

void UDisplaceModule::SetZDisplaceModule(UNoiseModule* DisplaceModule)
{
	Modules[3] = DisplaceModule;
}

int32 UDisplaceModule::GetSourceModuleCount() const
{
	return 4;
}

float UDisplaceModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	check(Modules[1] != nullptr);
	check(Modules[2] != nullptr);
	check(Modules[3] != nullptr);

	float xDisplace = X + Modules[1]->GetValue(X, Y, Z);
	float yDisplace = Y + Modules[2]->GetValue(X, Y, Z);
	float zDisplace = Z + Modules[3]->GetValue(X, Y, Z);

	return Modules[0]->GetValue(xDisplace, yDisplace, zDisplace);
}
