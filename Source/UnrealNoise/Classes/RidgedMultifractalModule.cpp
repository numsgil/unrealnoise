#include "UnrealNoisePrivatePCH.h"
#include "RidgedMultifractalModule.h"

URidgedMultifractalModule::URidgedMultifractalModule()
{
	CalculateSpectralWeights();
}

URidgedMultifractalModule::~URidgedMultifractalModule()
{
}

int32 URidgedMultifractalModule::GetSourceModuleCount() const
{
	return 0;
}

void URidgedMultifractalModule::CalculateSpectralWeights()
{
	float h = 1.f;
	float frequency = 1.f;

	for (int i = 0; i < 30; i++)
	{
		// Compute weight for each frequency.
		SpectralWeights[i] = FMath::Pow(frequency, -h);
		frequency *= Lacunarity;
	}
}

float URidgedMultifractalModule::GetValue(float X, float Y, float Z) const
{
	X *= Frequency;
	Y *= Frequency;
	Z *= Frequency;

	float signal = 0.f, value = 0.f, weight = 1.f;
	float offset = 1.f, gain = 2.f;

	for (int curOctave = 0; curOctave < OctaveCount; curOctave++)
	{
		// Make sure that these floating-point values have the same range as a 32-
		// bit integer so that we can pass them to the coherent-noise functions.
		float nx = MakeInt32Range(X), ny = MakeInt32Range(Y), nz = MakeInt32Range(Z);

		// Get the coherent-noise value.
		int32 seed = (Seed + curOctave) & 0x7fffffff;
		signal = GradientCoherentNoise3D(nx, ny, nz, seed);

		// Make the ridges.
		signal = FMath::Abs(signal);
		signal = offset - signal;

		// Square the signal to increase the sharpness of the ridges.
		signal *= signal;

		// The weighting from the previous octave is applied to the signal.
		// Larger values have higher weights, producing sharp points along the
		// ridges.
		signal *= weight;

		// Weight successive contributions by the previous signal.
		weight = signal * gain;
		if (weight > 1.0) weight = 1.0;
		if (weight < 0.0) weight = 0.0;

		// Add the signal to the output value.
		value += (signal * SpectralWeights[curOctave]);

		// Go to the next octave.
		X *= Lacunarity;
		Y *= Lacunarity;
		Z *= Lacunarity;
	}

	return (value * 1.25f) - 1.f;
}
