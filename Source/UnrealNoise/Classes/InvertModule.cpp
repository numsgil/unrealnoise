#include "UnrealNoisePrivatePCH.h"
#include "InvertModule.h"

UInvertModule::UInvertModule() : UNoiseModule(GetSourceModuleCount())
{}

UInvertModule::~UInvertModule()
{}

int32 UInvertModule::GetSourceModuleCount() const
{
	return 1;
}

float UInvertModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	return -Modules[0]->GetValue(X, Y, Z);
}
