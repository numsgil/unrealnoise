#include "UnrealNoisePrivatePCH.h"
#include "ClampModule.h"

UClampModule::UClampModule() : UNoiseModule(GetSourceModuleCount())
{
}

UClampModule::~UClampModule()
{
}

int32 UClampModule::GetSourceModuleCount() const
{
	return 1;
}

float UClampModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	float value = Modules[0]->GetValue(X, Y, Z);
	if (value < LowerBound)
		return LowerBound;
	if (value > UpperBound)
		return UpperBound;
	return value;
}
