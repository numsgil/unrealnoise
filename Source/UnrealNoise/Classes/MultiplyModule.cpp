#include "UnrealNoisePrivatePCH.h"
#include "MultiplyModule.h"

UMultiplyModule::UMultiplyModule() : UNoiseModule(GetSourceModuleCount())
{
}

UMultiplyModule::~UMultiplyModule()
{
}

int32 UMultiplyModule::GetSourceModuleCount() const
{
	return 2;
}

float UMultiplyModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	check(Modules[1] != nullptr);
	return Modules[0]->GetValue(X, Y, Z) * Modules[1]->GetValue(X, Y, Z);
}
