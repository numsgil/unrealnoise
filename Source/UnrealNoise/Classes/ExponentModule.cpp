#include "UnrealNoisePrivatePCH.h"
#include "ExponentModule.h"

UExponentModule::UExponentModule() : UNoiseModule(GetSourceModuleCount())
{}

UExponentModule::~UExponentModule()
{}

int32 UExponentModule::GetSourceModuleCount() const
{
	return 1;
}

float UExponentModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	float value = Modules[0]->GetValue(X, Y, Z);
	return FMath::Pow(FMath::Abs((value + 1.f) / 2.f), Exponent) * 2.f - 1.f;
}
