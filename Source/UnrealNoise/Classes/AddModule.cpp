#include "UnrealNoisePrivatePCH.h"
#include "AddModule.h"

UAddModule::UAddModule() : UNoiseModule(GetSourceModuleCount())
{
}

UAddModule::~UAddModule()
{
}

int32 UAddModule::GetSourceModuleCount() const
{
	return 2;
}

float UAddModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	check(Modules[1] != nullptr);
	return Modules[0]->GetValue(X, Y, Z) + Modules[1]->GetValue(X, Y, Z);
}
