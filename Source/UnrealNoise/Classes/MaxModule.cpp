#include "UnrealNoisePrivatePCH.h"
#include "MaxModule.h"

UMaxModule::UMaxModule() : UNoiseModule(GetSourceModuleCount())
{
}

UMaxModule::~UMaxModule()
{
}

int32 UMaxModule::GetSourceModuleCount() const
{
	return 2;
}

float UMaxModule::GetValue(float X, float Y, float Z) const
{
	check(Modules[0] != nullptr);
	check(Modules[1] != nullptr);

	return FMath::Max(Modules[0]->GetValue(X, Y, Z), Modules[1]->GetValue(X, Y, Z));
}
